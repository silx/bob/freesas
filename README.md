# FreeSAS
[![pipeline status](https://gitlab.esrf.fr/silx/bob/freesas/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/freesas/pipelines)

Automatic packaging for the Free Small Angle Scattering software suite


Build binary packages for [FreeSAS](https://github.com/kif/freesas): [Latest build](https://silx.gitlab-pages.esrf.fr/bob/freesas)
